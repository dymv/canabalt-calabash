class Jumper
  def initialize(player_position, current_building, next_building, player_velocity)
    @player_position = player_position
    @current_building = current_building
    @next_building = next_building
    @player_velocity = player_velocity
    @score = 0
    @jumps = 0
  end

  def heights_landing_const
    next_jump_small? ? 12 : 17
  end

  def landing_const
    next_jump_small? ? 1.7 : 1.4
  end

  def heights_diff
    Float(@next_building.height_i - @current_building.height_i)
  end

  def next_jump_small?
    heights_diff < 50
  end

  def heights_landing_factor
    factor = 1.0

    if next_jump_small?
      factor = heights_diff.abs / @player_velocity * heights_landing_const

      if factor < 1.0
        factor = 1.0
      end
    end

    factor
  end

  def landing_position
    @player_position.x_i + @player_velocity/landing_const * heights_landing_factor
  end

  def jump_zone
    landing_position - @next_building.x_i
  end

  def gap
    @next_building.x_i - (@current_building.x_i + @current_building.width_i)
  end

  def walking_factor
    @player_velocity/(gap/(heights_diff.abs + 0.1))
  end

  def what_should_i_do?
    result = :nothing
    if jump_zone > 0 && @player_position.x_i >= jump_zone
      if next_jump_small? || heights_landing_factor > 1.0

        if heights_diff < -30 && walking_factor > 200
          result = :just_walk_trust_me
        else
          result = :small_jump
          @jumps += 1
        end
      else
        @jumps += 1
        result = :medium_jump
      end
    end

    result
  end

  def alive?
    @player_position.x_i != 0 || @player_position.y_i != 0
  end

end