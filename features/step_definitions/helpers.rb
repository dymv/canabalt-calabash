def player_score
  ps = backdoor("clb_getPlayerScore:", "")
  Integer(ps)
end

def player_position
  pps = backdoor("clb_getPlayerPosition:","")
  splitted = pps.split('|')
  Point.new(splitted[0],splitted[1])
end

def player_velocity
  Integer(backdoor("clb_getPlayerVelocity:",""))
end

def cbuilding_rect
  cbp = backdoor("clb_getBuildingPosition:","0")
  splitted = cbp.split('|')
  Rect.new(splitted[0],splitted[1],splitted[2],splitted[3])
end

def nbuilding_rect
  nbp = backdoor("clb_getBuildingPosition:","1")
  splitted = nbp.split('|')
  Rect.new(splitted[0],splitted[1],splitted[2],splitted[3])
end