require "features/step_definitions/helpers.rb"
require "features/step_definitions/point.rb"
require "features/step_definitions/rect.rb"
require "features/step_definitions/jumper.rb"

Given /^I am on the Welcome Screen$/ do
  element_exists("view")
  sleep(STEP_PAUSE)
end


def jump_till_death
  jumps = 0

  while true
    j = Jumper.new(player_position, cbuilding_rect, nbuilding_rect, player_velocity)

    action = j.what_should_i_do?

    case action
      when :small_jump
        playback "s_jump"
      when :medium_jump
        playback "m_jump"
      else
        #doing nothing
    end

    unless j.alive?
      break
    end
  end

  ps = player_score

  {:score => ps, :jumps => jumps}
end

Then /^I play Canabalt$/ do
  pp = player_position
  px = Integer(pp.xpos);
  py = Integer(pp.ypos);

  if px == 0 && py == 0
    playback "start_game"
    sleep(0.1)

    number_of_plays = 1
    current_play = Integer(0)
    total_score = Integer(0)
    total_jumps = Integer(0)

    while true
      res = jump_till_death

      current_score = res[:score]
      current_jumps = res[:jumps]

      total_score += current_score
      total_jumps += current_jumps

      "Game: #{current_play} finished with score: #{current_score} jumps: #{current_jumps}\n".display

      current_play += 1

      if current_play >= number_of_plays
        break
      end

      sleep(1.0)
      playback "start_game"
      sleep(1.0)

    end

    avg_score = total_score / Float(number_of_plays)
    avg_jumps = total_jumps / Float(number_of_plays)

    puts "Average score: #{avg_score}"
    puts "Average jumps for game: #{avg_jumps}"

  end

  sleep(STEP_PAUSE)
end
