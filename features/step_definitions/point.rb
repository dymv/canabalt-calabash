class Point
  attr_accessor :ypos, :xpos

  def initialize(x, y)
    @xpos = x
    @ypos = y
  end

  def x_i
    Integer(@xpos)
  end

  def y_i
    Integer(@ypos)
  end

  def display
    puts "x:#{@xpos}, y:#{@ypos}"
  end
end
