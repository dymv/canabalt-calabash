class Rect

  attr_accessor :ypos, :xpos, :height, :width

  def initialize(x,y,w,h)
    @xpos = x
    @ypos = y
    @width = w
    @height = h
  end

  def x_i
    Integer (@xpos)
  end

  def y_i
    Integer (@ypos)
  end

  def width_i
    Integer (@width)
  end

  def height_i
    Integer (@height)
  end

  def display
    puts "x:#{@xpos}, y:#{@ypos}, width:#{@width}, height:#{@height}"
  end
end