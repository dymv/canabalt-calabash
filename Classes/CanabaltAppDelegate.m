//
//  CanabaltAppDelegate.m
//  Canabalt
//
//  Copyright Semi Secret Software 2009-2010. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "CanabaltAppDelegate.h"
#import "Canabalt.h"

#import <SemiSecret/SemiSecretTexture.h>

void preloadTextureAtlases()
{
  NSDictionary * infoDictionary = nil;
  if (FlxG.iPad)
    infoDictionary = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], @"iPadTextureAtlas.atlas"]];
  else
    infoDictionary = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], @"textureAtlas.atlas"]];

  //can only contain NSData, NSDate, NSNumber, NSString, NSArray, and NSDictionary

  NSDictionary * images = [infoDictionary objectForKey:@"images"];

  for (NSString * image in images) {
    NSDictionary * imageInfo = [images objectForKey:image];
    CGRect placement;
    placement.origin.x = [[imageInfo objectForKey:@"placement.origin.x"] floatValue];
    placement.origin.y = [[imageInfo objectForKey:@"placement.origin.y"] floatValue];
    placement.size.width = [[imageInfo objectForKey:@"placement.size.width"] floatValue];
    placement.size.height = [[imageInfo objectForKey:@"placement.size.height"] floatValue];
    NSString * atlas = [imageInfo objectForKey:@"atlas"];
    SemiSecretTexture * textureAtlas = [FlxG addTextureWithParam1:atlas param2:NO];
    SemiSecretTexture * texture = [SemiSecretTexture textureWithAtlasTexture:textureAtlas
						     offset:placement.origin
						     size:placement.size];
    [FlxG setTexture:texture forKey:image];
  }

}

@implementation CanabaltAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    [self dropPlayerPosition];
    [self dropBuildingsRects];
    [self dropPlayerScore];


  //in canabalt, we never want linear filtering (not even on ipad)
  [SemiSecretTexture setTextureFilteringMode:SSTextureFilteringNearest];
  
  [application setStatusBarOrientation:UIInterfaceOrientationLandscapeRight
	       animated:NO];

  game = [[Canabalt alloc] init];

  //preload textures here, now that opengl stuff should be created
  preloadTextureAtlases();
  
  return YES;
}

- (void) applicationDidEnterBackground:(UIApplication *)application
{
  [FlxG didEnterBackground];
}

- (void) applicationWillEnterForeground:(UIApplication *)application
{
  [FlxG willEnterForeground];
}

- (void) applicationWillResignActive:(UIApplication *)application
{
  [FlxG willResignActive];
}

- (void) applicationDidBecomeActive:(UIApplication *)application
{
  [FlxG didBecomeActive];
}

- (void) applicationWillTerminate:(UIApplication *)application
{
}

+ (CanabaltAppDelegate *)sharedInstance {
    return (CanabaltAppDelegate *) [[UIApplication sharedApplication] delegate];
}

- (void)dropPlayerPosition {
    if (fabsf(self.playerPosition.x) > 1e-5) {
        [self setPlayerScore:(NSInteger) (self.playerPosition.x / 10.0)];
    }
    [self setPlayerPosition:CGPointZero];
}

- (void)dropBuildingsRects {
    [self setCurrentBuildingRect:CGRectZero];
    [self setNextBuildingRect:CGRectZero];
}

- (void)dropPlayerScore {
    [self setPlayerScore:0];
}

- (void)addBuildingRect:(CGRect)buildingRect {
    if (CGRectEqualToRect(self.currentBuildingRect, CGRectZero)) {

        [self setCurrentBuildingRect:buildingRect];

    } else if (CGRectEqualToRect(self.nextBuildingRect, CGRectZero)) {

        [self setNextBuildingRect:buildingRect];

    } else {

        [self setCurrentBuildingRect:self.nextBuildingRect];
        [self setNextBuildingRect:buildingRect];

    }

//    NSLog(@"\ncurrent:%@\nnext%@\nplayer:%@", NSStringFromCGRect(self.currentBuildingRect), NSStringFromCGRect(self.nextBuildingRect), NSStringFromCGPoint(self.playerPosition));
}

- (NSString *)pointToS:(CGPoint)point {
    return [NSString stringWithFormat:@"%.0f|%.0f", point.x, point.y];
}

- (NSString *)rectToS:(CGRect)rect {
    return [NSString stringWithFormat:@"%.0f|%.0f|%.0f|%.0f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height];
}

- (NSString *)clb_getPlayerScore:(NSString *)attributes {
    return [NSString stringWithFormat:@"%d", self.playerScore];
}

- (NSString *)clb_getPlayerPosition:(NSString *)attributes {
    return [self pointToS:self.playerPosition];
}

- (NSString *)clb_getPlayerVelocity:(NSString *)attributes {
    return [NSString stringWithFormat:@"%.0f", self.playerVelocity];
}

- (NSString *)clb_getBuildingPosition:(NSString *)attributes {
    NSInteger attr_i = [attributes integerValue];
    switch (attr_i) {
        case 0: {
            return [self rectToS:self.currentBuildingRect];
        }
        case 1: {
            return [self rectToS:self.nextBuildingRect];
        }
        default:
            return @"";
    }
}


- (void) dealloc
{
  [game release];
  [super dealloc];
}

@end
