//
//  CanabaltAppDelegate.h
//  Canabalt
//
//  Copyright Semi Secret Software 2009-2010. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import <UIKit/UIKit.h>
#import <Flixel/Flixel.h>

@class Canabalt;

@interface CanabaltAppDelegate : NSObject <UIApplicationDelegate>
{
  Canabalt * game;
}

@property (nonatomic, assign) NSInteger playerScore;
@property (nonatomic, assign) CGFloat playerVelocity;
@property (nonatomic, assign) CGPoint playerPosition;
@property (nonatomic, assign) CGRect currentBuildingRect;
@property (nonatomic, assign) CGRect nextBuildingRect;

+ (CanabaltAppDelegate *)sharedInstance;

- (void)addBuildingRect:(CGRect)buildingRect;
- (void)dropPlayerPosition;
- (void)dropBuildingsRects;

- (NSString *)clb_getPlayerScore:(NSString *)attributes;
- (NSString *)clb_getPlayerPosition:(NSString *)attributes;
- (NSString *)clb_getPlayerVelocity:(NSString *)attributes;
- (NSString *)clb_getBuildingPosition:(NSString *)attributes;

@end

